package com.huy;

import javax.net.ssl.SSLServerSocket;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

public class Main {
    final static int SERVER_PORT = 1337;
    // Store client's related information like username
    static ArrayList<CommunicateThread> clients = new ArrayList<>();
    // Store group's related information like group name, members
    static ArrayList<Group> groups = new ArrayList<>();
    static int lastID = 1;

    public static void main(String[] args) throws IOException {
        SSLServerSocket serverSocket = new SSLServerSocket(SERVER_PORT) {
            @Override
            public String[] getEnabledCipherSuites() {
                return new String[0];
            }

            @Override
            public void setEnabledCipherSuites(String[] strings) {

            }

            @Override
            public String[] getSupportedCipherSuites() {
                return new String[0];
            }

            @Override
            public String[] getSupportedProtocols() {
                return new String[0];
            }

            @Override
            public String[] getEnabledProtocols() {
                return new String[0];
            }

            @Override
            public void setEnabledProtocols(String[] strings) {

            }

            @Override
            public void setNeedClientAuth(boolean b) {

            }

            @Override
            public boolean getNeedClientAuth() {
                return false;
            }

            @Override
            public void setWantClientAuth(boolean b) {

            }

            @Override
            public boolean getWantClientAuth() {
                return false;
            }

            @Override
            public void setUseClientMode(boolean b) {

            }

            @Override
            public boolean getUseClientMode() {
                return false;
            }

            @Override
            public void setEnableSessionCreation(boolean b) {

            }

            @Override
            public boolean getEnableSessionCreation() {
                return false;
            }
        };
        System.out.println("Server is up and waiting for new connection");
        while (true) {
            // Wait for an incoming client-connection request (blocking).
            Socket socket = serverSocket.accept();
            System.out.println("New client connected");

            // Start a message processing thread for each connecting client.
            CommunicateThread communicateThread = new CommunicateThread(socket);
            clients.add(communicateThread);
            Thread t1 = new Thread(communicateThread);

            // Start a ping thread for each connecting client.
            PingThread pingThread = new PingThread(communicateThread);
            Thread t2 = new Thread(pingThread);

            t1.start();
            t2.start();
            System.out.println("Number of clients: " + clients.size());
        }
    }
}
