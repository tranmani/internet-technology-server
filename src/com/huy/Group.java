package com.huy;

import java.util.ArrayList;

public class Group {
    private String groupName;
    private ArrayList<String> members = new ArrayList<>();

    public Group(String groupName, String member) {
        this.groupName = groupName;
        this.members.add(member);
    }

    public String getGroupName() {
        return groupName;
    }

    public ArrayList<String> getMembers() {
        return members;
    }

    public void addMembers(String member) {
        this.members.add(member);
    }

    public void removeMembers(String member) {
        this.members.remove(member);
    }
}
