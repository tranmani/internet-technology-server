package com.huy;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.huy.Main.clients;

public class CommunicateThread implements Runnable {
    BufferedReader reader = null;
    PrintWriter writer = null;
    Socket socket;
    InputStream inputStream;
    OutputStream outputStream;
    String line = null;
    String username;
    boolean pongOK = false;
    boolean changedUsername = false;

    public CommunicateThread(Socket socket) {
        this.socket = socket;
        this.username = "guest" + Main.lastID;
        Main.lastID++;
    }

    @Override
    public void run() {
        try {
            this.inputStream = new DataInputStream(socket.getInputStream());
            this.outputStream = socket.getOutputStream();
            this.reader = new BufferedReader(new InputStreamReader(inputStream));

            printToClient("Welcome to the server, type HELP for the full user manual");

            while (true) {
                while ((line = reader.readLine()) != null) {
                    // String after removed command part eg. HELO, BCST...
                    String userTyped = extractString(line);
                    // Could be group name or user name after remove last part
                    String firstPartString;
                    // Content after remove above 2 parts
                    String lastPartString;
                    switch (checkMessageType(line)) {
                        case "HELO":
                            serverPrint(line);
                            if (isValidString(userTyped)) {
                                if (checkDuplicateUserName(userTyped, clients)) {
                                    printToClient("-ERR username already exist");
                                } else {
                                    this.username = userTyped;
                                    changedUsername = true;
                                    printToClient("+OK " + userTyped);
                                    printToClient("+Your name is: " + this.username);
                                }
                            } else {
                                printToClient("-ERR username should be in the right format (only characters, numbers and underscores are allowed)");
                            }

                            System.out.println("----- Users list -----");
                            for (int i = 0; i < clients.size(); i++) {
                                System.out.println("+ " + clients.get(i).getUsername());
                            }
                            System.out.println("----------------------");
                            break;
                        case "BCST":
                            serverPrint(line);
                            if (changedUsername) {
                                for (CommunicateThread client : clients) {
                                    if (client != this) {
                                        client.printToClient("BCST [" + this.getUsername() + "] " + userTyped);
                                    }
                                }

                                printToClient("+OK " + line);
                            } else {
                                printToClient("-ERR you have to declare your username first by typing HELO *your_name*");
                            }
                            break;
                        case "CRGR":
                            // Create group command
                            // How to use: CRGR *group_name*
                            serverPrint(line);
                            if (changedUsername) {
                                if (isValidString(userTyped)) {
                                    if (!checkDuplicateGroupName(userTyped, Main.groups)) {
                                        for (CommunicateThread client : clients) {
                                            if (client == this) {
                                                Main.groups.add(new Group(userTyped, this.username));
                                            }
                                        }
                                        printToClient("+OK " + userTyped);
                                        printToClient("+Your group name is: " + userTyped);
                                    } else {
                                        printToClient("-ERR group name already exist");
                                    }
                                } else {
                                    printToClient("-ERR group name should be in the right format (only characters, numbers and underscores are allowed)");
                                }
                            } else {
                                printToClient("-ERR you have to declare your username first by typing HELO *your_name*");

                            }

                            System.out.println("----- Group list -----");
                            for (int i = 0; i < Main.groups.size(); i++) {
                                System.out.println("+ " + Main.groups.get(i).getGroupName() + ": " + Main.groups.get(i).getMembers());
                            }
                            System.out.println("----------------------");
                            break;
                        case "LSGR":
                            // Print list of groups command
                            // How to use: LSGR
                            serverPrint(line);
                            printListToClient("group");
                            break;
                        case "LSUS":
                            // Print list of users command
                            // How to use: LSUS
                            serverPrint(line);
                            printListToClient("user");
                            break;
                        case "JOIN":
                            // Join a group command
                            // How to use: JOIN *group_name*
                            serverPrint(line);
                            if (changedUsername) {
                                if (checkDuplicateGroupName(userTyped, Main.groups)) {
                                    for (int i = 0; i < Main.groups.size(); i++) {
                                        if (Main.groups.get(i).getGroupName().equals(userTyped)) {
                                            boolean exist = false;
                                            for (String member : Main.groups.get(i).getMembers()) {
                                                exist = member.equals(this.username);
                                            }
                                            if (exist) {
                                                printToClient("-ERR you are already a member of this group");
                                            } else {
                                                Main.groups.get(i).addMembers(this.username);
                                                printToClient("+OK you are now a member of group [" + userTyped + "]");
                                            }
                                            System.out.println("Member of this group: " + Main.groups.get(i).getMembers());
                                        }
                                    }
                                } else {
                                    printToClient("-ERR group name provided is not exist");
                                }
                            } else {
                                printToClient("-ERR you have to declare your username first by typing HELO *your_name*");
                            }
                            break;
                        case "GOUT":
                            // Leave group command
                            // How to use: GOUT *group_name*
                            serverPrint(line);
                            if (changedUsername) {
                                if (checkDuplicateGroupName(userTyped, Main.groups)) {
                                    for (int i = 0; i < Main.groups.size(); i++) {
                                        if (Main.groups.get(i).getGroupName().equals(userTyped)) {
                                            boolean exist = false;
                                            for (String member : Main.groups.get(i).getMembers()) {
                                                exist = member.equals(this.username);
                                            }
                                            if (exist) {
                                                Main.groups.get(i).removeMembers(this.username);
                                                printToClient("+OK leave group [" + userTyped + "] successfully");
                                            } else {
                                                printToClient("-ERR you are not a member of this group");
                                            }
                                            System.out.println("Members of this group: " + Main.groups.get(i).getMembers());
                                        }
                                    }
                                } else {
                                    printToClient("-ERR group name provided is not exist");
                                }
                            } else {
                                printToClient("-ERR you have to declare your username first by typing HELO *your_name*");
                            }
                            break;
                        case "DMGR":
                            // Send message to group
                            // How to use: DMGR *group_name* *message*
                            serverPrint(line);
                            firstPartString = extractGroupNameOrUserName(userTyped);
                            lastPartString = extractString(userTyped);
                            if (changedUsername) {
                                if (checkDuplicateGroupName(firstPartString, Main.groups)) {
                                    for (int i = 0; i < Main.groups.size(); i++) {
                                        if (Main.groups.get(i).getGroupName().equals(firstPartString)) {
                                            boolean ok = false;
                                            ArrayList<String> members = new ArrayList<>();
                                            for (int j = 0; j < Main.groups.get(i).getMembers().size(); j++) {
                                                members.add(Main.groups.get(i).getMembers().get(j));
                                                ok = Main.groups.get(i).getMembers().get(j).equals(this.username);
                                            }
                                            if (ok) {
                                                for (String member : members) {
                                                    for (CommunicateThread client : clients) {
                                                        if (client.getUsername().equals(member) && !client.getUsername().equals(this.username)) {
                                                            client.printToClient("+OK user [" + this.username + "] to group [" + firstPartString + "]: " + lastPartString);
                                                        }
                                                    }
                                                }
                                                printToClient("+OK you to group [" + firstPartString + "]: " + lastPartString);
                                            } else {
                                                printToClient("-ERR you are not a member of this group");
                                            }
                                        }
                                    }
                                } else {
                                    printToClient("-ERR group name provided is not exist");
                                }
                            } else {
                                printToClient("-ERR you have to declare your username first by typing HELO *your_name*");
                            }
                            break;
                        case "DMUS":
                            // Send message to user
                            // How to use: DMUS *user_name* *message*
                            serverPrint(line);
                            firstPartString = extractGroupNameOrUserName(userTyped);
                            lastPartString = extractString(userTyped);
                            if (changedUsername) {
                                if (checkDuplicateUserName(firstPartString, clients)) {
                                    printToClient("+OK message sent");
                                    CommunicateThread targetUser = findUser(firstPartString);
                                    targetUser.printToClient("+OK user [" + this.username + "]: " + lastPartString);
                                } else {
                                    printToClient("-ERR username provided is not exist");
                                }
                            } else {
                                printToClient("-ERR you have to declare your username first by typing HELO *your_name*");
                            }
                            break;
                        case "KICK":
                            // Kick a user out of a group
                            // How to use: KICK *group_name* *user_name*
                            serverPrint(line);
                            firstPartString = extractGroupNameOrUserName(userTyped);
                            lastPartString = extractString(userTyped);
                            if (changedUsername) {
                                if (checkDuplicateGroupName(firstPartString, Main.groups)) {
                                    if (checkDuplicateUserName(lastPartString, clients)) {
                                        for (int i = 0; i < Main.groups.size(); i++) {
                                            if (Main.groups.get(i).getGroupName().equals(firstPartString)) {
                                                if (Main.groups.get(i).getMembers().get(0).equals(this.username)) {
                                                    boolean exist = false;
                                                    for (String member : Main.groups.get(i).getMembers()) {
                                                        exist = member.equals(lastPartString);
                                                    }
                                                    if (exist) {
                                                        Main.groups.get(i).removeMembers(lastPartString);
                                                        printToClient("+OK removed user [" + lastPartString + "] from group [" + firstPartString + "]");
                                                        // notify the kicked user that he gets kicked from the group
                                                        for (int j = 0; j < clients.size(); j++) {
                                                            if (clients.get(j).getUsername().equals(lastPartString)) {
                                                                clients.get(j).printToClient("+OK you were kicked by [" + this.username + "] from group [" + firstPartString + "]");
                                                                break;
                                                            }
                                                        }
                                                        System.out.println("Members of this group: " + Main.groups.get(i).getMembers());
                                                    } else {
                                                        printToClient("-ERR username provided is not a member of this group");
                                                    }
                                                } else {
                                                    printToClient("-ERR only group owner can kick other user");
                                                }
                                            }
                                        }
                                    } else {
                                        printToClient("-ERR username provided is not exist");
                                    }
                                } else {
                                    printToClient("-ERR group name provided is not exist");
                                }
                            } else {
                                printToClient("-ERR you have to declare your username first by typing HELO *your_name*");
                            }
                            break;
                        case "SEND":
                            // Send a file to another user
                            // How to use: SEND *user_name* *file_name_with_extension_in_C_drive*
                            serverPrint(line);
                            firstPartString = extractGroupNameOrUserName(userTyped);
                            lastPartString = extractString(userTyped);

                            if (changedUsername) {
                                if (checkDuplicateUserName(firstPartString, clients)) {
                                    CommunicateThread targetUser = findUser(firstPartString);
                                    Thread.sleep(50);
                                    FileThread fileThread = new FileThread(true, lastPartString);
                                    Thread t = new Thread(fileThread);
                                    t.start();

                                    int sleep = 2000;
                                    if (lastPartString.contains(".jpg") || lastPartString.contains(".png") || lastPartString.contains(".tif") || lastPartString.contains(".gif")) sleep = 1000;
                                    else if (lastPartString.contains(".zip") || lastPartString.contains(".mp4") || lastPartString.contains(".exe")) sleep = 3000;
                                    Thread.sleep(sleep);

                                    targetUser.printToClient("+OK user [" + this.username + "]: Send you a file (" + lastPartString + ")");
                                    FileThread fileThread2 = new FileThread(false, lastPartString);
                                    Thread t2 = new Thread(fileThread2);
                                    t2.start();

                                    printToClient("+OK receiver have received your file");
                                } else {
                                    printToClient("-ERR username provided is not exist");
                                }
                            } else {
                                printToClient("-ERR you have to declare your username first by typing HELO *your_name*");
                            }

                            break;
                        case "PONG":
                            this.pongOK = true;
                            System.out.println("[" + username + "] is still alive");
                            break;
                        case "QUIT":
                            serverPrint(line);
                            removeConnection();
                            break;
                        case "HELP":
                            serverPrint(line);
                            printManual();
                            break;
                        case "UNKNOWN":
                            serverPrint(line);
                            printToClient("[ERROR] Unknown command");
                    }
                }
            }
        } catch (IOException | InterruptedException e) {
            System.out.println("Connection is terminated by user: " + username);
        }
    }

    public void removeConnection() throws IOException {
        PingThread.pongAvailable = false;
        clients.remove(this);
        this.socket.close();
    }

    public void printToClient(String mess) {
        writer = new PrintWriter(outputStream);
        writer.println(mess);
        writer.flush();
    }

    private String checkMessageType(String mess) {
        String result;
        String type = mess.substring(0, 4);
        if (type.equals("HELO")) {
            result = "HELO";
        } else if (type.equals("BCST")) {
            result = "BCST";
        } else if (type.equals("PONG")) {
            result = "PONG";
        } else if (type.equals("QUIT")) {
            result = "QUIT";
        } else if (type.equals("CRGR")) {
            result = "CRGR";
        } else if (type.equals("LSGR")) {
            result = "LSGR";
        }  else if (type.equals("LSUS")) {
            result = "LSUS";
        } else if (type.equals("DMUS")) {
            result = "DMUS";
        } else if (type.equals("JOIN")) {
            result = "JOIN";
        } else if (type.equals("GOUT")) {
            result = "GOUT";
        } else if (type.equals("DMGR")) {
            result = "DMGR";
        } else if (type.equals("SEND")) {
            result = "SEND";
        } else if (type.equals("KICK")) {
            result = "KICK";
        } else if (type.equals("HELP")) {
            result = "HELP";
        } else {
            result = "UNKNOWN";
        }
        return result;
    }

    public String getUsername() {
        return username;
    }

    private String extractString(String mess) {
        return mess.substring(mess.indexOf(" ") + 1);
    }

    private String extractGroupNameOrUserName(String mess) {
        return mess.substring(0, mess.indexOf(" "));
    }

    private boolean isValidString(String string) {
        Pattern pattern = Pattern.compile("[a-zA-Z0-9_]{3,14}");
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }

    private boolean checkDuplicateUserName(String userName, ArrayList<CommunicateThread> clients) {
        boolean result = false;
        for (CommunicateThread client : clients) {
            if (client.getUsername() == null) {
                result = false;
            } else {
                if (client.getUsername().equals(userName)) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    private CommunicateThread findUser(String userName) {
        CommunicateThread result = null;
        for (CommunicateThread client : clients) {
            if (client.getUsername().equals(userName)) {
                result = client;
                break;
            }
        }
        return result;
    }

    private boolean checkDuplicateGroupName(String groupName, ArrayList<Group> groups) {
        boolean result = false;
        for (Group group : groups) {
            if (group.getGroupName() != null) {
                if (group.getGroupName().equals(groupName)) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    private void serverPrint (String mess) {
        System.out.println("User [" + this.username + "] type: " + mess);
    }

    private void printListToClient(String type) {
        writer = new PrintWriter(outputStream);
        if (type.equals("user")) {
            writer.println("----- Users list -----");
            for (int i = 0; i < clients.size(); i++) {
                writer.println("+ " + clients.get(i).getUsername());
            }
        } else if (type.equals("group")) {
            writer.println("----- Groups list ----");
            for (int i = 0; i < Main.groups.size(); i++) {
                writer.println("+ " + Main.groups.get(i).getGroupName() + ": " + Main.groups.get(i).getMembers());
            }
        }
        writer.println("----------------------");
        writer.flush();
    }

    private void printManual() {
        writer = new PrintWriter(outputStream);
        writer.println("******************************************************************************************");
        writer.println("- Type [HELO *user_name*] to change your default username");
        writer.println("- Type [BCST *message*] to broadcast your message to all users");
        writer.println("- Type [LSGR] to display list of all groups");
        writer.println("- Type [LSUS] to display list of all users");
        writer.println("- Type [CRGR *group_name*] to create a new group");
        writer.println("- Type [KICK *group_name* *user_name*] to kick other user out of your own group,");
        writer.println("only owner of the group can kick other member of the group, if you quit your own group,");
        writer.println("the second member who joined your group will be owner of that group");
        writer.println("- Type [JOIN *group_name*] to join group");
        writer.println("- Type [GOUT *group_name*] to leave group");
        writer.println("- Type [DMGR *group_name* *message*] to broadcast your message to all group members");
        writer.println("- Type [DMUS *user_name* *message*] to send a direct message to other user");
        writer.println("- Type [SEND *user_name* *file_name_with_extension_in_C_drive*] to send a file to other user");
        writer.println("- Type [QUIT] to quit this server");
        writer.println("- Type [PONG] to to keep your connection with the server");
        writer.println("******************************************************************************************");
        writer.flush();
    }
}
