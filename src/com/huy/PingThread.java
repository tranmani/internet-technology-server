package com.huy;

import java.io.IOException;

public class PingThread implements Runnable {
    CommunicateThread communicateThread;
    static boolean pongAvailable = true;

    public PingThread(CommunicateThread communicateThread) {
        this.communicateThread = communicateThread;
    }

    @Override
    public void run() {
        while (pongAvailable) {
            try {
                Thread.sleep(10000);
                this.communicateThread.pongOK = false;
                this.communicateThread.printToClient("PING");
                Thread.sleep(3000);

                if (!this.communicateThread.pongOK) {
                    this.communicateThread.printToClient("DSCB PING timeout");
                    this.communicateThread.removeConnection();
                }
            } catch (InterruptedException | IOException e) {
                System.out.println("Can't send PING message");
            }
        }
    }
}
