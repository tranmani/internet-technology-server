package com.huy;

import javax.net.ssl.SSLServerSocket;
import java.io.*;
import java.net.Socket;

import static com.huy.FileThread.*;

public class FileThread implements Runnable {
    final static int SERVER_PORT_RECEIVE = 6969;
    static String fileName = null;
    static SSLServerSocket serverSocket = null;
    boolean toReceive;

    public FileThread (boolean toReceive, String fileName) {
        this.toReceive = toReceive;
        FileThread.fileName = fileName;
    }

    @Override
    public void run() {
        try {
            serverSocket = new SSLServerSocket(SERVER_PORT_RECEIVE) {
                @Override
                public String[] getEnabledCipherSuites() {
                    return new String[0];
                }

                @Override
                public void setEnabledCipherSuites(String[] strings) {

                }

                @Override
                public String[] getSupportedCipherSuites() {
                    return new String[0];
                }

                @Override
                public String[] getSupportedProtocols() {
                    return new String[0];
                }

                @Override
                public String[] getEnabledProtocols() {
                    return new String[0];
                }

                @Override
                public void setEnabledProtocols(String[] strings) {

                }

                @Override
                public void setNeedClientAuth(boolean b) {

                }

                @Override
                public boolean getNeedClientAuth() {
                    return false;
                }

                @Override
                public void setWantClientAuth(boolean b) {

                }

                @Override
                public boolean getWantClientAuth() {
                    return false;
                }

                @Override
                public void setUseClientMode(boolean b) {

                }

                @Override
                public boolean getUseClientMode() {
                    return false;
                }

                @Override
                public void setEnableSessionCreation(boolean b) {

                }

                @Override
                public boolean getEnableSessionCreation() {
                    return false;
                }
            };
            System.out.println("File server is up and waiting for new connection");
            while (true) {
                // Wait for an incoming client-connection request (blocking).
                Socket socket = serverSocket.accept();
                System.out.println("New client connected to file server");

                if (toReceive) {
                    new ReceiveFile(socket).run();
                } else {
                    new SendFile(socket).run();
                }
            }
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int copy(InputStream in, OutputStream out) throws IOException {
        byte[] buf = new byte[2048];
        int bytesRead = 0;
        int totalBytes = 0;
        while((bytesRead = in.read(buf)) != -1) {
            totalBytes += bytesRead;
            out.write(buf, 0, bytesRead);
        }
        return totalBytes;
    }
}

class ReceiveFile {
    Socket socket;

    public ReceiveFile (Socket socket) {
        this.socket = socket;
    }

    void run() throws IOException {
        File dir = new File("C://temp/");
        dir.mkdirs();
        String fullFilePath = "C://temp/" + fileName;
        OutputStream out = new FileOutputStream(fullFilePath);
        InputStream in = socket.getInputStream();

        int bytesReceive = copy(in, out);
        System.out.println(String.format("%d bytes received.", bytesReceive));

        out.close();
        in.close();
        serverSocket.close();
    }
}

class SendFile {
    Socket socket;

    public SendFile (Socket socket) {
        this.socket = socket;
    }

    void run() throws IOException {
        OutputStream out = socket.getOutputStream();

        String fullFilePath = "C://temp/" + fileName;
        InputStream in = new BufferedInputStream(new FileInputStream(fullFilePath));
        int bytesSent = copy(in, out);
        System.out.println(String.format("%d bytes sent.", bytesSent));

        out.close();
        in.close();
        serverSocket.close();
    }
}